FactoryBot.define do
  factory :article do
    title 'Test title'
    text 'Test text'
    user_id '1'
  end
end
