# frozen_string_literal: true

require 'rails_helper'

describe Article, type: :model do
  describe 'associations' do
    it { should have_many(:comments) }
  end

  describe 'length' do
    it { should validate_length_of(:title).is_at_most(140) }
    it { should validate_length_of(:text).is_at_most(4000) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:text) }
  end

  describe '#subject' do
    it 'returns the article title' do
      # create article instance in tricky way
      article = FactoryBot.build(:article, title: 'Test title', text: 'Test title')
      expect(article.subject).to eq 'Test title'
    end
  end
end
