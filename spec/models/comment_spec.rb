# frozen_string_literal: true

require 'spec_helper'
require 'rails_helper'

describe Comment, type: :model do
  describe 'associations' do
    it { should belong_to(:article) }
  end

  describe 'length' do
    it { should validate_length_of(:body).is_at_most(4000) }
  end
end
