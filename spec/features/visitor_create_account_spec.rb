require 'spec_helper'
require 'rails_helper'
require 'session_helper'
require 'database_cleaner_helper'

feature 'Account creation' do
  scenario 'allows guest to create account' do
    sign_up
    expect(page).to have_content I18n.t 'devise.registrations.signed_up'
  end
end
