require 'rails_helper'
require 'database_cleaner_helper'

feature 'Comment creation' do
  before(:each) do
    sign_up
    article_create
  end

  scenario 'article show link present in list after article creation' do
    visit articles_path
    find_link('Show').visible?
  end

  scenario 'user interacts with article show link' do
    visit articles_path
    click_link('Show')
    expect(page).to have_content I18n.t('articles.new_comment')
  end

  scenario 'user prints comment text and clicks add comment button' do
    visit articles_path
    click_link('Show')
    fill_in 'comment_body',	with: 'somecommenttext'
    click_button('Create Comment')
    expect(page).to have_content I18n.t('articles.comment_created')
  end
end
