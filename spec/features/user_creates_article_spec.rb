require 'spec_helper'
require 'rails_helper'
require 'session_helper'
require 'database_cleaner_helper'
require 'article_helper'

feature 'Article creation' do
  before(:each) do
    sign_up
  end

  scenario 'allows user to vist new article page' do
    visit new_article_path
    expect(page).to have_content I18n.t('articles.new_article')
  end

  scenario 'allows user to create new article' do
    article_create
    expect(page).to have_content I18n.t('articles.article_created')
  end
end
