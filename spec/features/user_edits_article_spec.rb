require 'spec_helper'
require 'rails_helper'
require 'session_helper'
require 'database_cleaner_helper'

feature 'Artile editing' do
  before(:each) do
    sign_up
    article_create
  end

  scenario 'go to all articles page' do
    visit articles_path
    expect(page).to have_content I18n.t('articles.article_list')
  end

  scenario 'article editing link present in list after article creation' do
    visit articles_path
    find_link('Edit').visible?
  end

  scenario 'user interacts with edit link' do
    visit articles_path
    click_link('Edit')
    expect(page).to have_content I18n.t('articles.article_edit')
  end

  scenario 'user fills article edit fields with new data' do
    visit articles_path
    article_edit
    expect(page).to have_content I18n.t('articles.article_updated')
  end
end
