def sign_up
  visit new_user_registration_path
  fill_in 'user_email',	with: 'user@mail.com'
  fill_in 'user_username',	with: 'someusername'
  fill_in 'user_password',	with: 'secretpassword'
  fill_in 'user_password_confirmation',	with: 'secretpassword'
  click_button 'Sign up'
end
