def article_create
  visit new_article_path
  fill_in 'article_title',	with: 'sometext'
  fill_in 'article_text',	with: 'sometext'
  click_button 'Create Article'
end

def article_edit
  visit articles_path
  click_link('Edit')
  fill_in 'article_title',	with: 'uptadedtext'
  fill_in 'article_text',	with: 'uptadedtext'
  click_button 'Update Article'
end
