class ArticlesController < ApplicationController
  before_action :authenticate_user!, except: %i[show index]
  before_action :find_articles, only: %i[index destroy]
  before_action :find_article, only: %i[show edit update destroy]
  before_action :article_owner, only: %i[edit update destroy]

  def index; end

  def edit; end

  def update
    if @article.update(article_params)
      redirect_to @article
      flash[:success] = t('articles.article_updated')
    else
      render 'edit'
    end
  end

  def new; end

  def show; end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if @article.save
      flash[:success] = t('articles.article_created')
      redirect_to @article
    else
      render 'new'
    end
  end

  def destroy
    if @article.destroy
      render 'index'
    else
      redirect_to @article
    end
  end

  private

  def article_owner
    unless @article.user_id == current_user.id
      flash[:warning] = 'Access denied, as you are not article author'
      redirect_to articles_path
    end
  end

  def article_params
    params.require(:article).permit(:title, :text, :user_id)
  end

  def find_article
    if Article.exists?(params[:id])
      @article = Article.find(params[:id])
    else
      render 'new'
    end
  end

  def find_articles
    @articles = Article.all
  end
end
